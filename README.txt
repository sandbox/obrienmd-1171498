Builds a Chrome-specific HTML5 multi-upload dropzone on a specified DOM object.

If a user is of a given role, files dropped into the specifed DOM object will be
added to the Drupal files table, then added to a CCK Filefield on a given node.

usage: 
function dnd_multi_upload_build($selectortype, $selector, $nid, $filefield, $role = NULL)

$selectortype = Type of dropzone you'll be using. Either 'class' or 'id'.
$selector = The CSS class (should be unique) or id of the DOM element that is to become an HTML5 dropzone.
$nid = The nid of the node to which filefields will be added.
$filefield = The filefield to add to.
$role = Only users of this role will be able to access this functionality.
