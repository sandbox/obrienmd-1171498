Drupal.behaviors.dnd_multi_upload = function () {
  // Setup selectors and dropzone DOM object.
  switch (Drupal.settings.dnd_multi_upload.selectortype) {
  case 'id':
    window.dropZoneid = '#' + Drupal.settings.dnd_multi_upload.selector; // Bring in drop zone ID from Drupal vars, setup a version for jquery selection
    var dropZone = document.getElementById(Drupal.settings.dnd_multi_upload.dropzoneid);
  $(window.dropZoneid).addClass('dnd-multi-dropzone');
    break;
  case 'class':
    $('.' + Drupal.settings.dnd_multi_upload.selector).attr('id', 'dropZoneDefaultId');
    window.dropZoneid = '#dropZoneDefaultId';
    var dropZone = document.getElementById('dropZoneDefaultId');
    $(window.dropZoneid).addClass('dnd-multi-dropzone');
    break;
  default:
    break;
  }
    
  // Init variable to store dropzone when replacing w/ drophere and progress effects, and variable to guard in-progress uploads.
  window.dropZoneTemp = '';
  window.dropZoneInProgress = 0;
  
  // Setup the dnd listeners.
  window.addEventListener('dragover', handleDragOver, false);
  window.addEventListener('dragleave', handleDragStop, false);
  window.addEventListener('drop', handleDragStop, false);
  dropZone.addEventListener('drop', handleFileSelect, false); 
}

function handleDragOver(evt) {
  // Stop normal browser response.
  evt.stopPropagation();
  evt.preventDefault();
  
  // Check if an upload is in progress. If not, continue.
  if (window.dropZoneInProgress != 1) {
  
    // Save and replace current dropzone contents, set class to change style.
    if (window.dropZoneTemp == '') {
      window.dropZoneTemp = $(window.dropZoneid).html();
      var width = $(window.dropZoneid).width();
      var height = $(window.dropZoneid).height();
      $(window.dropZoneid).html('');
      $(window.dropZoneid).width(width);
      $(window.dropZoneid).height(height);
      $(window.dropZoneid).addClass('drophere');
    }
  }
}

function handleDragStop(evt) {
  // Check if an upload is in progress. If not, continue.
  if (window.dropZoneInProgress != 1) {
    // Replace dropzone contents with original, remove class to revert style to default.
    if (window.dropZoneTemp != '') {
      $(window.dropZoneid).html(window.dropZoneTemp);
      $(window.dropZoneid).width('auto');
      $(window.dropZoneid).height('auto');
      $(window.dropZoneid).removeClass('drophere');
      window.dropZoneTemp = '';
    }
  }
}

function handleFileSelect(evt) {
  // Stop normal browser response.
  evt.stopPropagation();
  evt.preventDefault();
  
  $(window.dropZoneid).removeClass('drophere'); // Remove hovered class from dropzone.
  
  // Check if an upload is in progress. If not, continue.
  if (window.dropZoneInProgress != 1) {
  window.dropZoneInProgress = 1;
  
  var files = evt.dataTransfer.files; // FileList object.
    
  var xhr = new XMLHttpRequest(); // Init upload XHR request.
  
  // Set XHR base URL, bring in params from Drupal var.
    if (Drupal.settings.dnd_multi_upload.purl) {
      var purl = '/' + Drupal.settings.dnd_multi_upload.purl 
    }
    else {
      var purl = '';
    }
    var baseUrl = '/dnd_multi_upload_upload';
    var params = Drupal.settings.dnd_multi_upload.nid + '/' + Drupal.settings.dnd_multi_upload.filefield;
    var url = purl + baseUrl + '/' + params;
    
    // Set initial uploading html and class.
    $(window.dropZoneid).html('')
    $(window.dropZoneid).addClass('uploading');
    
    $(window.dropZoneid).append('<div id=\'dnd-multi-dropzone-progress\'></div>'); // Add progress report subdiv.
    
    // As upload progresses, modify progress subdiv with kB and percentage.
    xhr.upload.addEventListener('progress',function(ev) {
      $('#dnd-multi-dropzone-progress').html(
        (ev.loaded / 1000).toFixed(0).toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ",") + ' of ' + (ev.total/1000).toFixed(0).toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ",") + ' kB (' + (ev.loaded/ev.total*100).toFixed(0) + '%)'
      );
    }, false);
    
    // When upload completes, fade out and remove progress subdiv, then restyle dropzone to default
    // style, replace dropzoneid contents with temp storage contents, and set temp storage to empty.
    xhr.upload.addEventListener('load',function(ev) {
      $("#dnd-multi-dropzone-progress").remove();
      $(window.dropZoneid).html(window.dropZoneTemp);
      $(window.dropZoneid).width('auto');
      $(window.dropZoneid).height('auto');
      $(window.dropZoneid).removeClass('uploading');
      window.dropZoneTemp = '';
      window.dropZoneInProgress = 0;
    }, false);
    
    // Open POST, add files, submit
    xhr.open('POST', url, true);
    var data = new FormData();
    for(var i = 0; i < files.length; i++) data.append('file'+i, files[i]);
    xhr.send(data);
    
    // @todo Need to make this more flexible, right now it's hardcoded to work with a view only (reload a view which is a dropzone).
    xhr.onload = function() {
      $(window.dropZoneid).html(xhr.responseText);
      Drupal.attachBehaviors(); // Call behaviors again to fully 'update' DOM
    }
  }
}